<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compare_results', function (Blueprint $table) {
            $table->dropColumn('matched_status');
            $table->dropColumn('matched_begins');
            $table->dropColumn('matched_par_1');
            $table->dropColumn('matched_par_2');
            $table->dropColumn('not_matched_par_1');
            $table->dropColumn('not_matched_par_2');

            $table->text('result_body')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compare_results', function (Blueprint $table) {
            $table->text('matched_status')->nullable();
            $table->text('matched_begins')->nullable();
            $table->text('matched_par_1')->nullable();
            $table->text('matched_par_2')->nullable();
            $table->text('not_matched_par_1')->nullable();
            $table->text('not_matched_par_2')->nullable();
            $table->dropColumn('result_body');
        });
    }
};
