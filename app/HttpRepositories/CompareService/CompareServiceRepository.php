<?php

namespace App\HttpRepositories\CompareService;

use App\Enum\ComparisonStatusEnum;
use App\Models\CompareResult;
use \Exception;
use App\HttpRepositories\CompareService\DTOS\SendFileDTO;
use App\HttpRepositories\CompareService\Requests\CompareResultSaveRequest;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CompareServiceRepository implements CompareServiceRepositoryInterface
{
    private PendingRequest $httpClient;
    private ?string $accessToken = null;
    private ?string $refreshToken = null;

    private int $tries = 0;
    private const MAX_TRIES = 3;

    public function __construct()
    {
        $this->httpClient = Http::withoutVerifying()->baseUrl(config('external_services.python_comparison.domain') . '/api');
    }

    /**
     * @throws Exception
     */
    public function upload(SendFileDTO $data)
    {
        return $this->makeRequest('post', 'comparison/create/', $data->toArray(), $data->getFiles());
    }

    /**
     * @param CompareResultSaveRequest $request
     * @return CompareResult
     */
    public function saveCompareResult(CompareResultSaveRequest $request): CompareResult
    {
        $changes = $request->input('result.changes');
        $doc_1 = $request->input('result.unique.doc_1');
        $doc_2 = $request->input('result.unique.doc_2');
        $result_body = "";
        $uniques_file_1 = implode("\n", $doc_1);
        $uniques_file_2 = implode("\n", $doc_2);
        foreach ($changes as $index => $item) {
            if ($index == 0) {
                $result_body .= $item;
                continue;
            }
            if (str_starts_with($item, 'В пункте')) {
                $result_body .= "\n\n" . $item;
            } else {
                $result_body .= "\n" . $item;
            }
        }

        $compare_result = new CompareResult();
        $compare_result->id_comparison = $request->input('external_id');
        $compare_result->result_body = $result_body;
        $compare_result->uniques_first = $uniques_file_1;
        $compare_result->uniques_second = $uniques_file_2;
        $compare_result->save();
        $compare_result->comparison->status = ComparisonStatusEnum::ready;
        $compare_result->comparison->save();
        Log::channel('daily')->info('external-request', [
            'data' => $request->all(),
        ]);
        return $compare_result;
    }

    /**
     * @param $method
     * @param $url
     * @param array $data
     * @param array $files
     * @return mixed
     * @throws Exception
     */
    private function makeRequest($method, $url, array $data = [], array $files = []): mixed
    {
        if ($this->tries === self::MAX_TRIES) {
            throw new Exception('Can\'t authorize.');
        }

        if (!$this->accessToken) {
            $this->auth();
        }
        $this->httpClient = $this->httpClient->withHeaders([
            'Authorization' => 'Bearer ' . $this->accessToken,
        ]);

        if (count($files) > 0) {
            $this->httpClient->attach($files);
        }

        $response = $this->httpClient->$method($url, $data);

        Log::channel('daily')->info('request', [
            'url' => $url,
            'data' => $data,
            'status' => $response->status(),
        ]);

        if ($response->status() === 401) {
            $this->refresh();
            $this->tries += 1;
            return $this->makeRequest($method, $url, $data);
        }

        return $response;
    }

    /**
     * @return void
     */
    private function auth(): void
    {
        $response = $this->httpClient->post('token/', [
            'username' => config('external_services.python_comparison.username'),
            'password' => config('external_services.python_comparison.password')
        ]);
        if ($response->successful()) {
            $this->accessToken = $response->json('access');
            $this->refreshToken = $response->json('refresh');
        }
    }

    /**
     * @return void
     */
    private function refresh(): void
    {
        $response = $this->httpClient->post('token/refresh/', [
            'refresh' => $this->refreshToken,
        ]);
        if ($response->successful()) {
            $this->accessToken = $response->json('access');
            $this->refreshToken = $response->json('refresh');
        }
    }

}
