<?php

namespace App\HttpRepositories\CompareService\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompareResultSaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'status' => 'required|string',
            'external_id' => 'required|numeric|exists:comparisons,id',
            'result.unique.doc_1' => 'required|array',
            'result.unique.doc_1.*' => 'required|string',
            'result.unique.doc_2' => 'required|array',
            'result.unique.doc_2.*' => 'required|string',
            'result.changes' => 'required|array',
            'result.changes.*' => 'required|string',
        ];
    }
}
