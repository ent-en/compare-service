<?php

namespace App\Http\Controllers\Api;

use App\Events\DocumentComparedEvent;
use App\Http\Controllers\Controller;
use App\HttpRepositories\CompareService\CompareServiceRepositoryInterface;
use App\HttpRepositories\CompareService\Requests\CompareResultSaveRequest;
use Illuminate\Http\JsonResponse;

class CompareResultController extends Controller
{
    public function saveResult(
        CompareServiceRepositoryInterface $compareServiceRepository,
        CompareResultSaveRequest          $request
    ): JsonResponse
    {
        $compare_result = $compareServiceRepository->saveCompareResult($request);
        event(new DocumentComparedEvent($compare_result));
        return response()->json(["result" => "Success"]);
    }
}
