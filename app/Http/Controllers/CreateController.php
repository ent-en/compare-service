<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DocsRequest;
use App\Models\SourceDocument;
use App\Models\Comparison;

class CreateController extends Controller
{

    public function index(Request $request)
    {
        $comparisons = Comparison::query()->where('status', '=', 1)->get();
        $documents = SourceDocument::query()->get();

        return view('index', [
            'documents' => $documents,
            'comparisons' => $comparisons
        ]);
    }

    public function createComparison(DocsRequest $request)
    {
        $comparison = new Comparison();
        $comparison->name = "Сравненеие " . now()->format("dd-mm-YYYY");
        $comparison->comment = "";
        $comparison->save();
        dd($comparison);
        return redirect()->route('show-docs-id', [
            "id" => $comparison->id,
        ]);

    }

    public function getComparison($id)
    {
        $comparison = new Comparison();
        $comparison->name = "Сравненеие " . now()->format("dd-mm-YYYY");
        $comparison->comment = "";
        $comparison->save();

        return view('docupload', [
            "comparison" => $comparison
        ]);
    }
}
