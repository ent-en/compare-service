<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentToCompare
 * @property int $id
 * @property int $id_comparison
 * @property string|null $result_body
 * @property string|null $uniques_first
 * @property string|null $uniques_second
 * @property Comparison $comparison
 *
 **/
class CompareResult extends Model
{
    use HasFactory;

    public function comparison()
    {
        return $this->hasOne(Comparison::class, 'id', 'id_comparison');
    }
}
