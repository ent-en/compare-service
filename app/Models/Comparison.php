<?php

namespace App\Models;

use App\Enum\ComparisonStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class DocumentToCompare
 * @property int $id
 * @property string $name
 * @property string ?$comment
 * @property ComparisonStatusEnum $status
 * @property Collection|SourceDocument[] $sourceDocuments
 * @property Collection|DocumentsToCompare[] $compareDocuments
 * @property Collection|CompareResult[] $results
 *
 **/
class Comparison extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "comment",
        "status"
    ];
    protected $casts = [
        'status' => ComparisonStatusEnum::class
    ];

    public function sourceDocuments(): HasMany
    {
        return $this->hasMany(SourceDocument::class, 'id_comparison', 'id');
    }

    public function compareDocuments(): HasMany
    {
        return $this->hasMany(DocumentsToCompare::class, 'id_comparison', 'id');
    }

    public function results(): HasMany
    {
        return $this->hasMany(CompareResult::class, 'id_comparison', 'id')->orderBy('created_at', 'desc');;

    }

}
