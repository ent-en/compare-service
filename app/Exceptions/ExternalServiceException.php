<?php

namespace App\Exceptions;


use Illuminate\Http\JsonResponse;
use Throwable;

class ExternalServiceException extends \Exception
{
    public function __construct(string $message, int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return new JsonResponse(
            [
                'message' => $this->getMessage(),
            ],
            $this->getCode()
        );
    }
}
