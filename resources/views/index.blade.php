@extends('layouts.app')
@section('title', 'Список сравнений')
@section('body')
    <div class="container-fluid pb-5">
        <div class="banner">
            <img src="../images/banner/1350.jpg" alt="">
        </div>
        <div class="container mb-3">
            <div class="row mb-3">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Список</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col">
                    <div class="btn-add mb-3 d-flex justify-content-end">
                        <form action="{{ route('create.comparison') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-success">Добавить документ</button>
                        </form>
                    </div>
                    <table class="table table-warning">
                        <thead>
                        <tr>
                            <th class="text-center table-title w-5" scope="col">№</th>
                            <th class="text-center table-title w-50" scope="col">Name</th>
                            <th class="text-center table-title w-20" scope="col">Status</th>
                            <th class="text-center table-title w-20" scope="col">Date</th>
                        </tr>
                        </thead>
                        <tbody class="table-content">
                        @foreach ($comparisons as $item)
                            <tr>
                                <th class="table-content text-center w-5" scope="row">{{$item->id}}</th>
                                <td class="table-content w-50"><a
                                        href="{{route('edit.comparison',['comparison'=>$item->id])}}">{{$item->name}}</a>
                                </td>
                                <td class="table-content text-center w-20">
                                    @if($item->status == App\Enum\ComparisonStatusEnum::draft)
                                        <b>Новое сравнение</b>
                                    @elseif($item->status == App\Enum\ComparisonStatusEnum::ready)
                                        <b>Сравнение выполнено</b>
                                    @elseif($item->status == App\Enum\ComparisonStatusEnum::pending)
                                        <b>Сравнение выполняется, подождите ...</b>
                                    @endif
                                </td>
                                <td class="table-content text-center w-20">{{$item->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
