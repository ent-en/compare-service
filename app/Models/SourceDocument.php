<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SourceDocument
 * @property int $id
 * @property int $id_comparison
 * @property string $filename
 * @property string $path
 * @property Comparison|HasOne $comparison
 *
 **/
class SourceDocument extends Model
{
    use HasFactory;

    public function comparison()
    {
        return $this->hasOne(Comparison::class);
    }

}
