<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class DocumentToCompare
 * @property int $id
 * @property int $id_comparison
 * @property Comparison $comparison
 * @property string $matched_begins
 * @property string $matched_par_1
 * @property string $matched_par_2
 * @property string $not_matched_par_1
 * @property string $not_matched_par_2
 *
 **/
class ComparisonResult extends Model
{
    use HasFactory;

    public function comparison(): HasOne
    {
        return $this->hasOne(Comparison::class);
    }
}
