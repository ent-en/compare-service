<?php

return [
    'python_comparison' => [
        'domain' => env('COMPARE_SERVICE_URL'),
        'username' => env('COMPARE_SERVICE_USERNAME'),
        'password' => env('COMPARE_SERVICE_PASSWORD'),
        'callback_url' => env('COMPARE_SERVICE_CALLBACK'),
    ],
];
