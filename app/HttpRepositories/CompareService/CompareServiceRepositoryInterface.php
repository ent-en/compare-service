<?php

namespace App\HttpRepositories\CompareService;

use App\HttpRepositories\CompareService\DTOS\SendFileDTO;
use App\HttpRepositories\CompareService\Requests\CompareResultSaveRequest;
use App\Models\CompareResult;

interface CompareServiceRepositoryInterface
{
    /**
     * @param SendFileDTO $data
     */
    public function upload(SendFileDTO $data);

    /**
     * @param CompareResultSaveRequest $request
     */
    public function saveCompareResult(CompareResultSaveRequest $request): CompareResult;
}
