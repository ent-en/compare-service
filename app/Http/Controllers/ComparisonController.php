<?php

namespace App\Http\Controllers;

use App\Enum\ComparisonStatusEnum;
use App\Exceptions\ExternalServiceException;
use App\HttpRepositories\CompareService\CompareServiceRepositoryInterface;
use App\HttpRepositories\CompareService\DTOS\SendFileDTO;
use App\Models\Comparison;
use App\Models\DocumentsToCompare;
use App\Models\SourceDocument;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ComparisonController extends Controller
{
    public function index(): View
    {
        $comparisons = Comparison::query()->where('status', '!=', ComparisonStatusEnum::draft)->get();
        $documents = SourceDocument::query()->get();

        return view('index', [
            'documents' => $documents,
            'comparisons' => $comparisons
        ]);
    }

    public function create(): RedirectResponse
    {
        $comparison = Comparison::query()->create([
            "name" => "Сравненеие " . now()->format("d-m-Y"),
            "comment" => "",
        ]);
        return redirect()->route('edit.comparison', ['comparison' => $comparison]);
    }

    /**
     * @param Comparison $comparison
     * @return View
     */
    public function edit(Comparison $comparison): View
    {
        $comparison = Comparison::query()->where('id', $comparison->id)->with(['sourceDocuments', 'compareDocuments', 'results'])->first();
        return view('comparison.edit', compact('comparison'));
    }

    /**
     * @param Request $request
     * @param Comparison $comparison
     * @return RedirectResponse
     */
    public function update(Request $request, Comparison $comparison): RedirectResponse
    {
        $comparison->update([
            'name' => $request->input('name'),
            'comment' => $request->input('comment'),
            'status' => ComparisonStatusEnum::ready,
        ]);
        $sourceDocuments = $request->files->get('sourceDocuments') ?? [];
        $documentsToCompare = $request->files->get('documentsToCompare') ?? [];

        foreach ($sourceDocuments as $file) {
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('documents'), $fileName);

            $fileUpload = new SourceDocument();
            $fileUpload->filename = $fileName;
            $fileUpload->path = public_path('documents') . '/' . $fileName;
            $fileUpload->id_comparison = $comparison->id;
            $fileUpload->save();
        }

        foreach ($documentsToCompare as $file) {
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('documents'), $fileName);

            $fileUpload = new DocumentsToCompare();
            $fileUpload->filename = $fileName;
            $fileUpload->path = public_path('documents') . '/' . $fileName;
            $fileUpload->id_comparison = $comparison->id;
            $fileUpload->save();
        }

        return redirect()->route('edit.comparison', ['comparison' => $comparison->id]);
    }

    /**
     * @param Comparison $comparison
     * @param CompareServiceRepositoryInterface $compareServiceRepository
     * @return RedirectResponse
     * @throws ExternalServiceException
     */
    public function compareDocuments(
        Comparison                        $comparison,
        CompareServiceRepositoryInterface $compareServiceRepository
    ): RedirectResponse
    {
        $comparison = Comparison::query()
            ->where('id', $comparison->id)
            ->with(['sourceDocuments', 'compareDocuments', 'results'])
            ->first();
        try {
            $dto = new SendFileDTO(
                external_id: $comparison->id,
                url: route('compare.save-result'),
                source_files: $comparison->sourceDocuments,
                files_to_compare: $comparison->compareDocuments,
            );
            $compareServiceRepository->upload($dto);
            $comparison->update([
                'status' => ComparisonStatusEnum::pending,
            ]);
        } catch (\Exception $e) {
            throw new ExternalServiceException($e->getMessage());
        }
        return redirect()->route('edit.comparison', ['comparison' => $comparison->id]);
    }

    /**
     * @param SourceDocument $doc
     * @return JsonResponse
     */
    public function deleteSourceFile(SourceDocument $doc): JsonResponse
    {
        $doc->delete();
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param DocumentsToCompare $doc
     * @return JsonResponse
     */
    public function deleteCompareFile(DocumentsToCompare $doc): JsonResponse
    {
        $doc->delete();
        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
