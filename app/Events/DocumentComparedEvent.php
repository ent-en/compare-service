<?php

namespace App\Events;

use App\Models\CompareResult;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class DocumentComparedEvent implements ShouldBroadcast
{
    use SerializesModels;

    public function __construct(private readonly CompareResult $compare_result)
    {
    }

    public function broadcastOn(): Channel
    {
        return new Channel("compare-result.".$this->compare_result->comparison->id);
    }

    public function broadcastAs(): string
    {
        return 'updated';
    }
}
