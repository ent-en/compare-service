<?php

namespace App\HttpRepositories\CompareService\DTOS;

use Illuminate\Support\Collection;

class SendFileDTO
{
    public function __construct(
        private int        $external_id,
        private string     $url,
        private Collection $source_files,
        private Collection $files_to_compare,
    )
    {
    }

    public function getFiles(): array
    {
        $source_files = $this->source_files->map(fn($file) => ([
            'name' => 'source_files',
            'contents' => fopen($file->path, 'r')
        ]))->toArray();

        $files_to_compare = $this->files_to_compare->map(fn($file) => ([
            'name' => 'files_to_compare',
            'contents' => fopen($file->path, 'r')
        ]))->toArray();
        return [...$source_files, ...$files_to_compare];
    }

    /**
     * @return int
     */
    public function getExternalId(): int
    {
        return $this->external_id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            "external_id" => $this->getExternalId(),
            "url" => $this->getUrl(),
        ];
    }
}
