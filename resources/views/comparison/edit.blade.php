@extends('layouts.app')
@section('title', 'Сравнение')
@section('body')
    <div class="container-fluid pb-5">
        <div class="banner">
            <img src="../../images/banner/1350.jpg" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="nav nav-tabs mb-3">
                        <li class="nav-item">
                            <a class="nav-link " aria-current="page" href="{{route('index')}}">Список</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Сравнение</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="row mb-4">
                        <div class="accordion Warning" id="accordionPanelsStayOpenExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true"
                                            aria-controls="panelsStayOpen-collapseOne">
                                        Добавить сравнение
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseOne" class="accordion-collapse"
                                     aria-labelledby="panelsStayOpen-headingOne">
                                    <div class="accordion-body py-3">
                                        <div class="row">
                                            <div class="col-12 form-comparison">
                                                @if($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach($errors->all() as $error)
                                                                <li>
                                                                    {{$error}}
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif

                                                <form
                                                    id="update-form"
                                                    action="{{ route('update.comparison', ['comparison' => $comparison->id]) }}"
                                                    enctype="multipart/form-data"
                                                    method="post">
                                                    @csrf
                                                    <input name="name" value="{{ $comparison->name }}"
                                                           autofocus type="text" class="form-control mb-3"
                                                           placeholder="Название документа">
                                                    <textarea name="comment" class="form-control mb-3" cols="30"
                                                              rows="5"
                                                              placeholder="Комментарии">{{ $comparison->comment }}</textarea>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="mb-3">
                                                                <label for="formFile" class="form-label">Исходные
                                                                    файлы</label>
                                                                <input class="form-control mb-2"
                                                                       name="sourceDocuments[]"
                                                                       type="file"
                                                                       id="formFile" multiple>
                                                            </div>
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Название файла</th>
                                                                    {{-- <th scope="col">Ссылка</th> --}}
                                                                    <th scope="col">Дата</th>
                                                                    <th scope="col">Удалить</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($comparison->sourceDocuments as $item)
                                                                    <tr>
                                                                        <th scope="row">1</th>
                                                                        <td>{{ $item->filename }}</td>
                                                                        <td>{{ $item->created_at->format('d-m-Y h:i') }}</td>
                                                                        <td>
                                                                            <button class="btn btn-danger"
                                                                                    onclick="deleteFile('{{route('delete.source-file', ['doc' => $item->id]) }}')">
                                                                                Удалить
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="mb-3">
                                                                <label for="formFile" class="form-label">Файлы для
                                                                    сравнения</label>
                                                                <input class="form-control" name="documentsToCompare[]"
                                                                       type="file"
                                                                       id="formFile" multiple>
                                                            </div>
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">Название файла</th>
                                                                    {{-- <th scope="col">Ссылка</th> --}}
                                                                    <th scope="col">Дата</th>
                                                                    <th scope="col">Удалить</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($comparison->compareDocuments as $item)
                                                                    <tr>
                                                                        <th scope="row">1</th>
                                                                        <td>{{ $item->filename }}</td>
                                                                        <td>{{ $item->created_at->format('d-m-Y h:i') }}</td>
                                                                        <td>
                                                                            <button class="btn btn-danger"
                                                                                    onclick="deleteFile('{{route('delete.compare-file', ['doc' => $item->id])}}')">
                                                                                Удалить
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </form>

                                                <form
                                                    id="compare-form"
                                                    method="POST"
                                                    action="{{ route('compare.comparison', ['comparison' => $comparison->id]) }}">
                                                    @csrf
                                                </form>

                                                <div class="btn-add mb-3 d-flex justify-content-between">
                                                    <div class="d-flex justify-content-start">
                                                        <button class="btn btn-success mr-2" type="submit"
                                                                form="update-form">
                                                            Сохранить
                                                        </button>
                                                        <button class="btn btn-warning mx-2" type="submit"
                                                                form="compare-form">
                                                            Сравнить
                                                        </button>
                                                        <a href="{{route('edit.comparison',['comparison'=>$comparison->id])}}"
                                                           class="btn btn-secondary mx-2">
                                                            Обновить результаты сравнения
                                                        </a>
                                                    </div>
                                                    <div class="d-flex justify-content-end align-items-center">
                                                        <div>
                                                            @if($comparison->status == App\Enum\ComparisonStatusEnum::draft)
                                                                <span class="badge bg-primary">Новое сравнение</span>
                                                            @elseif($comparison->status == App\Enum\ComparisonStatusEnum::ready)
                                                                <span
                                                                    class="badge bg-success">Сравнение выполненно</span>
                                                            @elseif($comparison->status == App\Enum\ComparisonStatusEnum::pending)
                                                                <span class="badge text-dark bg-warning">Сравнение выполняется, подождите ...</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="result" class="container-fluid p-0">
                        @foreach ($comparison->results as $result)
                            <div class="row p-0 m-0">
                                <div class="col-12 p-0">
                                    <div class="card bg-secondary text-white mb-2">
                                        <div class="card-body pt-2 pb-1">
                                            <h5 class="card-title py-0">Результат сравнения
                                                от {{$result->created_at}} </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="card bg-light text-black mb-1">
                                        <div class="card-body">
                                            <h5 class="card-title">Результат сравнения</h5>
                                            <p class="card-text">{!! nl2br($result->result_body) !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="card bg-light text-black mb-1">
                                        <div class="card-body">
                                            <h5 class="card-title">Исходной файл</h5>
                                            <p class="card-text">{!! nl2br($result->uniques_first) !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="card bg-light text-black mb-1">
                                        <div class="card-body">
                                            <h5 class="card-title">Файл для сравнения</h5>
                                            <p class="card-text">{!! nl2br($result->uniques_second) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
    <script>
         function deleteFile(url) {
            if (confirm('Вы уверены?')) {
                $.ajax({
                    type: "delete",
                    url: url,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        location.reload();
                    },
                    error: function () {
                        alert("error");
                    }
                });
            }
         }
         Pusher.logToConsole = true;

         const pusher = new Pusher('63222ca0f5675786cb81', {
             cluster: 'ap2'
         });
         const channel = pusher.subscribe('compare-result.{{ $comparison->id }}');
         channel.bind('updated', function (data) {
             window.location.hash = '#result';
             location.reload();
         });
    </script>
@endsection
