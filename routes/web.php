<?php

use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ComparisonController;

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', [ComparisonController::class, 'index'])->name('index');
Route::post('comparison/create', [ComparisonController::class, 'create'])
    ->name('create.comparison');
Route::get('comparison/{comparison}/edit', [ComparisonController::class, 'edit'])
    ->name('edit.comparison');
Route::post('comparison/{comparison}/update', [ComparisonController::class, 'update'])
    ->name('update.comparison');
Route::post('comparison/{comparison}/compare', [ComparisonController::class, 'compareDocuments'])
    ->name('compare.comparison');

Route::withoutMiddleware([VerifyCsrfToken::class])->prefix('comparison/')->group(function () {
    Route::delete('source-files/{doc}', [ComparisonController::class, 'deleteSourceFile'])
        ->name('delete.source-file');
    Route::delete('compare-files/{doc}', [ComparisonController::class, 'deleteCompareFile'])
        ->name('delete.compare-file');
});
