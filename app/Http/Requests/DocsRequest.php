<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'create_comparison' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'create_comparison.required' => 'Поле название является обязательным'
        ];
    }
}
