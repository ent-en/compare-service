<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compare_results', function (Blueprint $table) {
            $table->text('uniques_first')->nullable();
            $table->text('uniques_second')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compare_results', function (Blueprint $table) {
            $table->dropColumn('uniques_first');
            $table->dropColumn('uniques_second');
        });
    }
};
