<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ComparisonResult;


class ResultController extends Controller
{
    public function showresult()
    {
        $compare_result = ComparisonResult::query()->all();


        return view('comparison.edit', [
            'compare_result' => $compare_result
        ]);
    }


}
