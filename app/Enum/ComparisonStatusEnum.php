<?php

namespace App\Enum;
enum ComparisonStatusEnum: int
{
    case draft = 1;
    case ready = 2;
    case pending = 3;
}
