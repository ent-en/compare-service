<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enum\ComparisonStatusEnum;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comparisons', function (Blueprint $table) {
            $table->id();
            $table->char('name', 255);
            $table->text('comment')->nullable();
            $table->enum('status', [ComparisonStatusEnum::draft->value, ComparisonStatusEnum::ready->value, ComparisonStatusEnum::pending->value]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('сomapre_results');
        Schema::dropIfExists('comparisons');
    }
};
