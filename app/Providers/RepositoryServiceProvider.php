<?php

namespace App\Providers;

use App\HttpRepositories\CompareService\CompareServiceRepository;
use App\HttpRepositories\CompareService\CompareServiceRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CompareServiceRepositoryInterface::class, CompareServiceRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
