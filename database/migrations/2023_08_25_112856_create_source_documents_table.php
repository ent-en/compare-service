<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('source_documents', function (Blueprint $table) {
            $table->id();
            $table->char('filename', 255);
            $table->text('path');
            $table->unsignedBigInteger('id_comparison');
            $table->foreign('id_comparison')->references('id')->on('comparisons');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_to_compares');
    }
};
